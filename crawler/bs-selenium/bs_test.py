#!/usr/bin/python
from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from bs4 import BeautifulSoup
import time
from tqdm import tqdm
import json
import re
# import requests
#
# headers = {'Upgrade-Insecure-Requests':'1',
#            'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36'}
# page_response = requests.get('https://www.twitter.com/xbox' , headers = headers)
# print(page_response)
# page_content = BeautifulSoup(page_response.content)
# print(page_content.prettify())

def get_tweet(testid):
	timeline_tweets = soup.find_all("div", {"data-testid": "%s" %testid})
	if(len(timeline_tweets) == 0):
		print("%s has no tweets" %target_page)
		return
	# print(type(timeline_tweets))
	tweet_list = []
	for tweets in timeline_tweets:
		tweet_info = {}
		#get tweet text
		tweets_html = BeautifulSoup(str(tweets), 'html.parser')
		# tweet_text = tweets_html.find("div", {
		# 	"class": "css-901oao r-18jsvk2 r-1qd0xha r-a023e6 r-16dba41 r-ad9z0x r-bcqeeo r-bnwqim r-qvutc0"}).text
		tweet_text = tweets_html.find("div", {"data-testid": "%s" %testid}).text
		#print("tweet text: " + tweet_text)
		if len(tweet_text)!= 0:
			tweet_info['text'] = tweet_text
		#get media
		videos = tweets_html.find_all("div", {"data-testid": "videoPlayer"})
		video_list = []
		for video in videos:
			video_html = BeautifulSoup(str(video),"html.parser")
			video_link = video_html.find("video")['src']
			video_list.append(video_link)
		if len(video_list) != 0:
			tweet_info['videos'] = video_list
		#print(video)
		photos = tweets_html.find_all("div", {"data-testid":"tweetPhoto"})
		photo_list = []
		for photo in photos:
			photo_html = BeautifulSoup(str(photo),"html.parser")
			photo_link = photo_html.find("img")['src']
			photo_list.append(photo_link)
		if len(photo_list) != 0:
			tweet_info['photos'] = photo_list
		#print(photo)
		#get_likes
		likes = tweets_html.find("div", {"data-testid": "like"}).text
		#print("likes: " , len(likes))
		if likes:
			tweet_info['likes'] = likes
		# get_replies
		replies = tweets_html.find("div", {"data-testid": "reply"}).text
		#print("replies: " , len(replies))
		if replies:
			tweet_info['replies'] = replies
		# get_retweets
		retweets = tweets_html.find("div", {"data-testid": "retweet"}).text
		#print("retweets: " , len(retweets))
		if retweets:
			tweet_info['retweets'] = retweets

		tweet_list.append(tweet_info)

	print(tweet_list)
	return tweet_list

#write to json file
def write_json(data, filename):
    with open(filename, 'w') as file:
        json.dump(data,file,indent=3)
#add data to json file
def append_json(bs_data,file_name):
	with open(file_name) as file:
		data = json.load(file)
		temp = data['bs_data']
		temp.append(bs_data)
	write_json(data,file_name)

target_page = input(" enter page ID : ")
chrome_options = webdriver.ChromeOptions()
# chrome_options.add_argument('--no-sandbox')
# chrome_options.add_argument('--window-size=1420,1080')
# chrome_options.add_argument('--headless')
# chrome_options.add_argument('--disable-gpu')
# driver = webdriver.Remote("http://127.0.0.1:4444/wd/hub", options=chrome_options)
driver = webdriver.Chrome('/Users/hanie/Desktop/chromedriver')
driver.get('https://twitter.com/%s' %target_page)

#wait until page loads
WebDriverWait(driver, 25).until(lambda x: x.find_element_by_css_selector("nav[aria-label='Profile timelines']"))

#scroll down the timeline
last_height = driver.execute_script("return document.body.scrollHeight")
SCROLL_PAUSE_TIME = 30
with tqdm() as pbar:
	while True:
		driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
		time.sleep(SCROLL_PAUSE_TIME)
		new_height = driver.execute_script("return document.body.scrollHeight")
		if new_height == last_height:
			break
		last_height = new_height
		pbar.update()

html_source = driver.page_source
sourcedata = html_source.encode('utf-8')
soup = BeautifulSoup(sourcedata, 'html.parser')
#get user name
user_name = soup.find("div", {"class": "css-1dbjc4n r-15d164r r-1g94qm0"})\
					.find_next("div", {"class":"css-901oao r-18jsvk2 r-1qd0xha r-1b6yd1w r-1vr29t4 r-ad9z0x r-bcqeeo r-qvutc0"})\
					.text
#print("user name: " + user_name)
#get user id
user_id = soup.find("div", {"class": "css-1dbjc4n r-15d164r r-1g94qm0"})\
					.find_next("div", {"class":"css-1dbjc4n r-18u37iz r-1wbh5a2"})\
					.text
#print("user id: " + user_id)
#
userProfileHeader_Items = soup.find("div", {"data-testid":"UserProfileHeader_Items"}).text
#print("UserProfileHeader_Items: " + userProfileHeader_Items)
#
tweet_numbers = soup.find("div", {"data-testid": "titleContainer"})\
						.find_next("div",{"class":"css-901oao css-bfa6kz r-m0bqgq r-1qd0xha r-n6v787 r-16dba41 r-1sf4r6n r-bcqeeo r-qvutc0"})\
						.text
#print("tweet numbers: " + tweet_numbers)
#get user description
try:
	user_description = soup.find("div", {"data-testid": "UserDescription"}).text
	#print("user description: " + user_description)
except:
	user_description = 'None'
	print("no description added for %s " %target_page)

#set user info dictionary
user_info = {'user_name' : user_name,
			 'user_id' : user_id,
			 'more_info' : userProfileHeader_Items,
			 'tweet_count' : tweet_numbers}

#check if description exits then add it to the dictionary
if user_description!="None":
	user_info["decription"] = user_description
print(user_info)

#get tweets
tweet_list = []
try:
	tweet_list = get_tweet("tweet")
except Exception as e:
	print(e)

#codes below are used when creating json file and adding the first user profile
#bs_data = {"bs_data": [{"user_info": user_info , "tweet_list": tweet_list}]}
#write_json(bs_data,'bs_data.json')

#codes below are used when json file already exists and we want to add more user profile
bs_data = {"user_info" : user_info , "tweet_list" : tweet_list}
append_json(bs_data,'bs_data.json')

driver.quit()