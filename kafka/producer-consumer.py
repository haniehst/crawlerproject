from time import sleep
import json
from json import dumps
from json import loads
from kafka import KafkaConsumer,KafkaProducer
import threading

class Producer(threading.Thread):

    def __init__(self):
        self.producer = KafkaProducer(bootstrap_servers=['localhost:9092'],
                                      value_serializer=lambda p:dumps(p).encode('utf-8'))
        super(Producer, self).__init__()
        self.file = open('/Users/hanie/PycharmProjects/crawlerproject/crawler/bs-selenium/bs_data.json')
    #send msg function
    def run(self):
        data = json.load(self.file)
        self.producer.send('rawtweets',value=data)
        sleep(1)

class Consumer(threading.Thread):

    def __init__(self):
        self.consumer = KafkaConsumer('rawtweets',
             bootstrap_servers=['localhost:9092'],
             auto_offset_reset='earliest',
             enable_auto_commit=True,
             #group_id='my-group',
             value_deserializer=lambda c: loads(c.decode('utf-8')))
        super(Consumer, self).__init__()
    #recieve msg function
    def run(self):
        for msg in self.consumer:
            message = msg.value
            print(message)

def run_threads():
    threads = [Producer(),Consumer()]
    for thread in threads:
        thread.start()
    sleep(10)

run_threads()

